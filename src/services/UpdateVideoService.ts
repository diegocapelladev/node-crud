import { getRepository } from "typeorm"
import { Video } from "../entities/Video"

type VideoUpdateRequest = {
  id: string
  name: string
  description: string
  duration: number
}

export class UpdateVideoService {
  async execute({ id, name, description, duration }: VideoUpdateRequest) {
    const repo = getRepository(Video)

    const video = await repo.findOne(id)

    if (!video) {
      return new Error("Video does not exists!")
    }

    video.name = name ? name : video.name
    video.description = description ? description : video.description
    video.duration = duration ? duration : video.duration

    await repo.save(video)

    return video
  } 
}