import { getRepository } from "typeorm";
import { Video } from "../entities/Video";


export class GetAllVideoService {
  async excute() {
    const repo = getRepository(Video)

    const videos = await repo.find({
      relations: ["category"]
    })

    return videos
  }
}