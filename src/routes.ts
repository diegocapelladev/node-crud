import { Router } from "express";
import { CreateCategoryController } from "./Controllers/CreateCategoryController";
import { CreateVideoController } from "./Controllers/CreateVideoController";
import { DeleteCategoryController } from "./Controllers/DeleteCategoryController";
import { DeleteVideoController } from "./Controllers/DeleteVideoController";
import { GetAllCategoryController } from "./Controllers/GetAllCategoryController";
import { GetAllVideoController } from "./Controllers/GetAllVideoController";
import { UpdateCategoryController } from "./Controllers/UpdateCategoryController";
import { UpdateVideoController } from "./Controllers/UpdateVideoControllers";

const routes = Router()

routes.post("/categories", new CreateCategoryController().handle)
routes.get("/categories", new GetAllCategoryController().handle)
routes.delete("/categories/:id", new DeleteCategoryController().handle)
routes.put("/categories/:id", new UpdateCategoryController().handle)

routes.post("/videos", new CreateVideoController().handle)
routes.get("/videos", new GetAllVideoController().handle)
routes.delete("/videos/:id", new DeleteVideoController().handle)
routes.put("/videos/:id", new UpdateVideoController().handle)

export {routes}