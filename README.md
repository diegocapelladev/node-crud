## Get Starting

- This project is using the PostgreSQL database

- First, create the local _.env_ file with the configuration listed below and set your database information to empty fields.

```
TYPEORM_CONNECTION = postgres
TYPEORM_HOST = localhost
TYPEORM_USERNAME = 
TYPEORM_PASSWORD = 
TYPEORM_DATABASE = 
TYPEORM_PORT = 5432
TYPEORM_MIGRATIONS = src/database/migrations/*.ts
TYPEORM_MIGRATIONS_DIR = src/database/migrations
TYPEORM_ENTITIES = src/entities/*ts
TYPEORM_ENTITIES_DIR = src/entities
```
